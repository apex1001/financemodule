<?php

	/**
	 * A scraping client for google financial services.
	 * Part of the CX-Ray app.
	 * 
	 * @author V. Vogelesang
	 * 
	 */
	class GoogleRateClient {
		
		/**
		 * Get the US Dollar > EUro exchange rate
		 * from google.
		 *
		 * @return exchange rate
		 */
		public function getUsdEurRate() {
			// Load Google conversion rate page into DOMdocument
			$url = 'http://www.google.com/finance?q=usdeur';
			$dom = new DOMDocument();
			$dom->strictErrorChecking = false;
			@$dom->loadHTMLFile($url);
		
			// Scrape for meta tage with itemprop = price
			foreach($dom->getElementsByTagName('meta') as $meta ) {
				if ($meta->getAttribute('itemprop') === 'price') {
					return $meta->getAttribute('content');
				}
			}
			return 1;
		}		
	}

?>
