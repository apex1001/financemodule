<?php

	/**
	 * A scraping client for companydetails.
	 * Part of the CX-Ray app.
	 * 
	 * @author V. Vogelesang
	 * 
	 */
	class CompanyDetailClient {
		
		/**
		 * Scrapes www.belegger.nl for stock name & location details
		 * for given company.
		 *
		 * @param company name @ www.belegger.nl
		 * @return company details array
		 */
		public function getCompanyDetails($name) {
			// Get all company details and add to detail array
			$detailNode = $this->getDetailNode($name);
			$detailArray = $this->getLocationDetails($detailNode);
			$detailArray['companyName'] = $name;
			$detailArray['stockName'] = $this->getStockName($detailNode);
			return $detailArray;
		}
		
		/**
		 * Get the DOMnode containing the company details
		 *
		 * @param company name
		 * @return DOMNode
		 */
		private function getDetailNode($name) {
			// Get the HTML document and surpress errors
			$url = 'http://www.belegger.nl/aandeel-' . $name . '.profiel';
			$dom = new DOMDocument();
			$dom->strictErrorChecking = false;
			@$dom->loadHTMLFile($url);
		
			// Get detail node from document
			$finder = new DomXPath($dom);
			$classname='twoColumns clearfix';
			$nodeList = $finder->query("//*[contains(@class, '$classname')]");
			// This is the root node for all details:
			$detailNode =  $nodeList->item(0);
			return $detailNode;
		}
		
		/**
		 * Get Stock name from element
		 *
		 * @param root detail element
		 * @return string stock name
		 */
		private function getStockName(DOMElement $detailNode) {
			// Get h5 node, contains stock name
			$nodeValue = $detailNode->getElementsByTagName('h5')->item(0)->nodeValue;
			$begin = strpos( $nodeValue,': ')+2;
			$end =  strpos( $nodeValue,' /');
			$stockName = substr($nodeValue, $begin, $end - $begin) . '.AS';
			return $stockName;
		}
		
		/**
		 * Get location details from element
		 *
		 * @param root detail element
		 * @return Array with Adress, Zipcode, City
		 */
		private function getLocationDetails(DOMElement $detailNode) {
			$locationDetails = array('address' => '', 'zipCode' => '', 'city' => '');
			$details = array('address' => 'Adres:', 'zipCode' => 'Postcode:', 'city' => 'Stad:');
			// Get all tbody's
			$bodyList = $detailNode->getElementsByTagName('tbody');
			if ($bodyList->length > 0) {
				//Get all rows from first tbody
				$rowList = $bodyList->item(0)->getElementsByTagName('tr');
				foreach($rowList as $row) {
					$header = $row->getElementsByTagName('th')->item(0)->nodeValue;
					$data = $row->getElementsByTagName('td')->item(0)->nodeValue;
					foreach($details as $key => $value) {
						if ($header == $value) {
							// Add found values to array
							$locationDetails[$key] = $data;
						}
					}
				}
			}
			return $locationDetails;
		}
	}

?>
