<?php

	/**
	 * A scraping client for company lists.
	 * Part of the CX-Ray app.
	 * 
	 * @author V. Vogelesang
	 * 
	 */
	class CompanyListClient {
		
		/**
		 * Scrapes www.belegger.nl for an AEX AMX AScX company list
		 *
		 * @return array with company names
		 */
		public function getCompanyList() {
		
			// Define exchanges we want to search
			$exchangeArray = array('AEX','AMX','ASCX');
				
			// Get the HTML document and surpress errors
			$companyArray = array();
			foreach($exchangeArray as $exchange) {
				$url = 'http://www.belegger.nl/koersen-' . $exchange . '.index';
				$dom = new DOMDocument();
				$dom->strictErrorChecking = false;
				@$dom->loadHTMLFile($url);
					
				// Get select list for stock from root node
				$selectNode = $dom->getElementById('stocknav');
				$companyElements = $selectNode->childNodes;
					
				// Add values to array
				foreach($companyElements as $item) {
					// Weed out comment elements
					if (strpos($item->nodeValue,'--') !== 0) {
						$companyName = str_replace(' ', '_', $item->nodeValue);
						array_push($companyArray, $companyName);
					}
				}
			}
			return $companyArray;
		}
				
	}

	?>
