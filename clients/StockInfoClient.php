<?php

	include_once 'GoogleRateClient.php';
	
	/**
	 * A scraping client for Stock information.
	 * Part of the CX-Ray app.
	 * 
	 * @author V. Vogelesang
	 * 
	 */
	class StockInfoClient {
		
		/**
		 * Gets the stock history from Yahoo finance and
		 * parses it into month => value format array.
		 *
		 * @param stockName
		 * @return stockHistoryArray
		 */
		public function getStockHistoryArray($stockName) {
			// Get the url for
			$yearString = $this->getYearString();
			$url = 'http://ichart.finance.yahoo.com/table.csv?s=' . $stockName . $yearString . '&g=m&ignore=.csv';
			$csvData = @file_get_contents($url);
			// In case of error, trigger dummy array
			if (!$csvData) {
				$dataArray = array();
				$dataArray[0] = null;
			}
			else $dataArray = str_getcsv(str_replace("\n",",",$csvData));
			return $this->parseDataArray($dataArray);
		
		}
		
		/**
		 * Formats the yearstring for Yahoo Finance API
		 * @return string
		 */
		private function getYearString() {
			$today = getdate();
			$month = sprintf("%02s", $today['mon']-1);
			$monthThen = sprintf("%02s", getdate(strtotime('+1 month'))['mon']-1);
			$year = $today['year'];
			$yearString = '&a=' . $monthThen . '&b=01&c=' . ($year - 1)  . '&d=' . $month . '&e=01&f=' . $year;
			return $yearString;
		}
		
		/**
		 * Get the monthly figures from the data
		 *
		 * @param dataArray
		 * @return stripped dataArray month => value format
		 */
		private function parseDataArray($dataArray) {
			// Get USD/EUR rate from google
			$googleClient = new GoogleRateClient();
			$rate = $googleClient->getUsdEurRate();
			
			// Return dummy array if no valid data found
			$stockHistoryArray = $this->fillDummyArray();
			if ($dataArray[0] === null || count($dataArray) < 80) {
				return $stockHistoryArray;
			}
					
			// Reverse array to count from 1 year ago to now
			$dataArray = array_reverse($dataArray);
			$j = getdate()['mon'];
		
			// Fill array with stock low for corresponding month
			for ($i = 4; $i < count($dataArray)-4; $i+=7) {
				$stockHistoryArray['m'.$j] = round ($dataArray[$i] * $rate, 2);
				$j = 1 + $j %12;
			}

			return $stockHistoryArray;
		}	
		
		/**
		 * Create a dummy array
		 * @return array
		 */
		private function fillDummyArray() {
			$stockHistoryArray = array();
			for ($i=1; $i < 13; $i++) {
					$stockHistoryArray['m'.$i] = 0;
				}
			return $stockHistoryArray;
		}		
	}
	
	


	
