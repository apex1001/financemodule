<?php

	/**
	 * An util class for CX-Ray
	 *  
	 * @author V. Vogelesang
	 *
	 */
	class FinanceUtils {
		
		/**
		 * Gets first word from company name
		 *
		 * @param name
		 * @return first part
		 */
		public function stripCompanyName($name) {
			$companyName = null;
			$name = str_replace('_', ' ',$name);
			$companyNameArray = explode(' ',trim($name));
			if (count($companyNameArray) > 0) {
				$companyName = $companyNameArray[0];
			}
			return $companyName;
		}
		
		/**
		 * Removes all spaces, dashes, slashes etc.
		 * and drops text to lowercase
		 *
		 * @param text
		 * @return cleaned text
		 */
		public function cleanString($text) {
			$text = str_replace(array(',', '.', '-', '_', '\\', '/', ' '),'',$text);
			return trim(strtolower($text));
		}
		
		/**
		 * Change old citynames to new
		 *
		 * @param city
		 * @return newcity
		 */
		public function getCity ($city) {
			if (strtolower(strstr($city, 'hertogenbos'))) $city = 'Den Bosch';
			if (strtolower(strstr($city, 'gravenhage'))) $city = 'Den Haag';
			return $city;
		}			
	}	
	
?>