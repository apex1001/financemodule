<?php

	require_once 'PHPUnit/Autoload.php';
	include_once 'FinanceModule.php';
	include_once 'FinanceUtils.php';
	include_once 'clients/CompanyListClient.php';
	include_once 'clients/CompanyDetailClient.php';	
	include_once 'clients/StockInfoClient.php';	
	
	/**
	 * Test class for the financemodule	 
	 * Tests all individual clients first, 
	 * then FinanceModule as a whole
	 * 
	 * @author V. Vogelesang
	 *
	 */
	class FinanceModuleTest extends PHPUnit_Framework_TestCase {

		public function testCompanyListClient() {

			// Arrange
			$client = new CompanyListClient();
	
			// Get Company List
			$array = $client->getCompanyList();
	
			// Assert given company's exists in list
			$this->assertEquals(84, count($array));
			$this->assertEquals("Aegon", $array[0]);
			$this->assertEquals("KPN", $array[15]);
			$this->assertEquals("Aperam", $array[30]);
			$this->assertEquals("AScX", $array[60]);
			$this->assertEquals("Xeikon", $array[83]);			
		}	
	
		public function testCompanyDetailClient() {
		
			// Arrange
			$client = new CompanyDetailClient();
		
			// Get Company Deatils
			$array = $client->getCompanyDetails("KPN");
		
			// Assert details for KPN
			$this->assertEquals(count($array), 5);
			$this->assertEquals("Maanplein 55", $array['address']);
			$this->assertEquals("2516 CR", $array['zipCode']);
			$this->assertEquals("Den Haag", $array['city']);
			$this->assertEquals("KPN", $array['companyName']);
			$this->assertEquals("KPN.AS", $array['stockName']);
		}
		
		public function testGoogleRateClient() {
		
			// Arrange
			$client = new GoogleRateClient();
		
			// Get Company Deatils
			$rate = $client->getUsdEurRate();	
		
			// Assert current rate between 0.5 and 1
			$this->assertGreaterThan(0.5, $rate);
			$this->assertLessThan(1, $rate);
		}
		
		public function testStockinfoClient() {
		
			// Arrange
			$client = new StockinfoClient();
		
			// Get Stock info for Reed Elsevier
			$array = $client->getStockHistoryArray("REN.AS");
		
			// Assert there is data coming from yahoo finance
			$this->assertGreaterThan(0, count($array));	
			$this->assertGreaterThan(0, $array['m5']);
		}	

		public function testFinanceModule() {
		
			// Arrange
			$fm = new FinanceModule();
				
			// Match adress
			$object1 = new stdClass();
			$object1->name = "Reed Elsevier";
			$object1->address = "Radarweg 29";
			$object1->zipCode = "1043 NX";
			$object1->city = "Amsterdam";
				
			// Match zipcode
			$object2 = new stdClass();
			$object2->name = "Reed Elsevierrrrr";
			$object2->address = "Radarweeeg 28";
			$object2->zipCode = "1043 NX";
			$object2->city = "Amsterdam";
				
			// Match first name part + city
			$object3 = new stdClass();
			$object3->name = "Reed Elsevierrrrr";
			$object3->address = "Radarweeeg 28";
			$object3->zipCode = "1045 NX";
			$object3->city = "Amsterdam";
			
			// Match nothing
			$object4 = new stdClass();
			$object4->name = "a";
			$object4->address = "a";
			$object4->zipCode = "a";
			$object4->city = "a";
		
			// Act
			$array1 = $fm->getStockHistory($object1);
			$array2 = $fm->getStockHistory($object2);
			$array3 = $fm->getStockHistory($object3);
			$array4 = $fm->getStockHistory($object4);
		
			// Assert all ojects match except object 4
			$this->assertGreaterThan(0, count($array1));
			$this->assertGreaterThan(0, $array1['m5']);
			$this->assertGreaterThan(0, count($array2));
			$this->assertGreaterThan(0, $array2['m5']);
			$this->assertGreaterThan(0, count($array3));
			$this->assertGreaterThan(0, $array3['m5']);
			$this->assertGreaterThan(0, count($array4));
			$this->assertEquals(0, $array4['m5']);
		}
	}
?>