<?php

	/*
	 * A SOAP server for getting Stock History for given company details
	 * Part of the CX-Ray app
	 * 
	 * @author V. Vogelesang
	 * 
	 */
 	include_once 'FinanceModule.php';	
		
	startServer("wsdl/financeModule.wsdl","getStockHistory");

	/**
	 * Start the SOAP server for the given method and wsdl file
	 * @param fileName
	 * @param methodName
	 */
	function startServer($fileName, $methodName) {
		ini_set("soap.wsdl_cache_enabled", "0"); // disabling WSDL cache
		$server = new SoapServer($fileName);
		$server->setClass('FinanceModule');
		$server->handle();
	}	
	
?>