<?php
	
	include_once 'clients/CompanyListClient.php';
	include_once 'clients/CompanyDetailClient.php';	
	include_once 'clients/StockInfoClient.php';
	include_once 'FinanceUtils.php';

	/**
	 * The finance module for getting Stock History for given company details.
	 * Part of the CX-Ray app.
	 *
	 * @author V. Vogelesang
	 */
	class FinanceModule {
		private $companyListClient;
		private $companyDetailClient;
		private $googleRateClient;
		private $stockInfoClient;
		private $financeUtils;
		
		function __construct() {
			$this->companyListClient = new CompanyListClient();
			$this->companyDetailClient = new CompanyDetailClient();
			$this->googleRateClient = new GoogleRateClient();
			$this->stockInfoClient = new StockInfoClient();
			$this->financeUtils = new FinanceUtils();
		}
		
		/**
		 * Get the Stock history for the given company
		 * 
		 * @param name
		 * @param address
		 * @param zipCode
		 * @param city
		 * @return stockHistoryArray
		 */
		public function getStockHistory($object) {	
			return $this->getStockInfo($object->name, $object->address, $object->zipCode, $object->city);
		}
	
		/**
		 * Get the stock info for a company
		 * @param name
		 * @param address
		 * @param zipcode
		 * @param city
		 * @return stockHistoryArray
		 */
		private function getStockInfo($name, $address, $zipcode, $city) {
			// Get companyDetailArray from memcache or create it
			$companyDetailArray = $this->getMemCache("companyDetailArray");
			if (!$companyDetailArray) {
				// Get CompanyList for AEX, AMX, AScX
				set_time_limit(200);
				$companyArray = $this->companyListClient->getCompanyList();
				
				// Get details for each company
				set_time_limit(200);
				$companyDetailArray = $this->getCompanyDetailArray($companyArray);
				
				// Save to memcache
				$this->setMemCache("companyDetailArray", $companyDetailArray);						
			}
			
			// Match given company to companyDetailArray, returns stockName		
			$stockName = $this->getCompanyStockName($name, $address, $zipcode, $city, $companyDetailArray);
			
			// Get stockHistoryArray for given stockName
			if ($stockName != null) {
				// Get the historical stock info
				set_time_limit(200);
				$stockArray = $this->stockInfoClient->getStockHistoryArray($stockName);			
			}
			
			// Fill empty array in case there is no stockName
			else {
				$stockArray = array();
				for ($i=1; $i < 13; $i++) {
					$stockArray['m'.$i] = 0;
				}
			}		
			return $stockArray;
		}
		
		/**
		 * Get the stockname for given company details and Array
		 * 
		 * @param name
		 * @param address
		 * @param zipcode
		 * @param companyDetailArray
		 * @return stock name
		 */
		private function getCompanyStockName($name, $address, $zipCode, $city, $companyDetailArray) {
			$stockName = null;
					
			// Iterate company detail array for a match, return stock name
			foreach($companyDetailArray as $company) {
				// Try address match first
				$tempAddress = $company['address'];
				if ($this->financeUtils->cleanString($address) === $this->financeUtils->cleanString($tempAddress) && $tempAddress !== '') {
					$stockName = $company['stockName'];
					break;	
				}
				
				// Match zipcode next 			
				$tempZipCode = $company['zipCode'];
				if ($this->financeUtils->cleanString($zipCode) === $this->financeUtils->cleanString($tempZipCode) && $tempZipCode !== '') {
					$stockName = $company['stockName'];
					break;			
				}				
				
				// If adress and zip match fails try _crude_ name/city match
				$tempCompanyName = $this->financeUtils->stripCompanyName($company['companyName']);
				$companyName = $this->financeUtils->stripCompanyName($name);
				if ($this->financeUtils->cleanString($companyName) === $this->financeUtils->cleanString($tempCompanyName)) {
					$tempCity = $this->financeUtils->getCity($company['city']);
					$city = $this->financeUtils->getCity($city);				
					if ($this->financeUtils->cleanString($city) === $this->financeUtils->cleanString($tempCity)) {
						$stockName = $company['stockName'];
					break;
					}
				}			
			}		
			return str_replace('E:','',$stockName);
		}
		
		/** Fill the company detail list
		 * 
		 * @param companyArray
		 * @return companyDetailArray
		 */
		private function getCompanyDetailArray($companyArray) {
			// Get details for each company
			$companyDetailArray = array();
			foreach ($companyArray as $company) {
				$detailArray = $this->companyDetailClient->getCompanyDetails($company);
				array_push($companyDetailArray, $detailArray);
			}
			return $companyDetailArray;		
		}
		
		/**
		 * Get object from memcache for given key
		 * 
		 * @param key
		 * @return object, false if not found
		 */	
		private function getMemCache($key) {
			$memcache = $this->createMemcache();
			return $memcache->get($key);		
		}
		
		/**
		 * Save object to memcache for given key
		 * 
		 * @param key, object
		 */
		private function setMemCache($key, $companyDetailArray) {
			$memcache = $this->createMemcache();
			$memcache->set($key,  $companyDetailArray, false, 85000) or die ("Failed to save data at the server");
		}
		
		/**
		 * Create memchache object
		 * 
		 * @return memcache object
		 */
		private function createMemcache() {
			$memcache = new Memcache;
			$memcache->connect('localhost', 11211) or die ("Could not connect");
			return $memcache;
		}	
	}
	
	
